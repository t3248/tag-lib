import csv


def csv_open(path, tag_list):
    """read a csv file and return contests as a list"""
    with open(path, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            tag_list.append(row)
        csv_file.close()


def csv_write(path, tag_list):
    with open(path, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file)
        for tag_line in tag_list:
            csv_writer.writerow(tag_line)
        csv_file.close()


def search(tag_list, search_item):
    """search tag_list for the tag"""
    flag = 0
    index = 0

    for line in tag_list:
        index += 1

        # checking string is present in line or not
        if search_item in line:
            flag = 1
            break

    # checking condition for string found or not
    if flag == 0:
        return [False, index]
    else:
        return [True, index]


def create_tag(tag_list, tag):
    """check before you create a new tag"""
    is_tag = search(tag_list, tag)

    if is_tag[0]:
        print(f'the tag ({tag}) already exists')
    else:
        tag_list.append([tag])


def remove_tag(tag_list, tag):
    """removes tag from list and moves any tagged files to untagged"""
    is_tag = search(tag_list, tag)

    if not is_tag[0]:
        print(f'\nthe tag: {tag}, du not exist')
    else:
        tag_position = tag_list[is_tag[1] - 1]

        for item in tag_position[1::]:
            tag_position.remove(item)
            item_elsewhere = search(tag_list, item)
            if item_elsewhere[0]:
                print(f'{item} was tagged elsewhere')
            else:
                tag_file(tag_list, 'untagged', item)

        tag_list.remove([tag])


def tag_file(tag_list, tag, file):
    """assigning a tag to a file"""
    is_tag = search(tag_list, tag)
    if is_tag[0]:
        print(f'\nthe tag {tag} was found')

    elif input(f'\nthe tag {tag} is not found in the tag_list du you want to create it? (y/N)') != 'y':
        print('closing')
        exit()

    else:
        print(f"writing {tag} to tag_list")
        tag_list.append([tag])
        is_tag = search(tag_list, tag)

    tag_position = tag_list[is_tag[1] - 1]
    is_file_tagged = search(tag_position, file)

    if is_file_tagged[0]:
        print(f'the {file} is already tagged as {tag_position[0]}')
    else:
        print(f"tagging {file} to {tag_position[0]}")
        tag_position.append(file)
